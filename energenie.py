#!/usr/bin/env python

# The On/Off code pairs correspond to the hand controller codes.
# True = '1', False ='0'
#
# "OUT OF THE BOX: Plug the Pi _transmitter board into the Raspberry Pi"
# "GPIO pin-header ensuring correct polarity and pin alignment."
# ""
# "The sockets will need to be inserted into separate mains wall sockets."
# "with a physical separation of at least 2 metres to ensure they don't"
# "interfere with each other. Do not put into a single extension lead."
# ""
# "For proper set up the sockets should be in their factory state with"
# "the red led flashing at 1 second intervals. If this is not the case for"
# "either socket, press and hold the green button on the front of the unit"
# "for 5 seconds or more until the red light flashes slowly."
# ""
# "A socket in learning mode will be listening for a control code to be"
# "sent from a _transmitter. A socket can pair with up to 2 _transmitters"
# "and will accept the following code pairs"
# ""
# "0011 and 1011 all sockets ON and OFF"
# "1111 and 0111 socket 1 ON and OFF"
# "1110 and 0110 socket 2 ON and OFF"
# "1101 and 0101 socket 3 ON and OFF"
# "1100 and 0100 socket 4 ON and OFF"
# ""
# "A socket in learning mode should accept the first code it receives"
# "If you wish the sockets to react to different codes, plug in and"
# "program first one socket then the other using this program."
# ""
# "When the code is accepted you will see the red lamp on the socket"
# "flash quickly then extinguish"

import RPi.GPIO as GPIO
from sys import argv
import time

class Energenie:

    codes = { "socket1" : { "ON": [1, 1, 1, 1], "OFF" : [0, 1, 1, 1] } }

    def __enter__(self):
        self.initialise_gpio()

    def __exit__(self, exception_type, exception_value, traceback):
        self.cleanup_gpio()


    def _transmit(self, code):
        # Set K0-K3
        GPIO.output (11, code[3])
        GPIO.output (15, code[2])
        GPIO.output (16, code[1])
        GPIO.output (13, code[0])
        # let it settle, encoder requires this
        time.sleep(0.1)	
        # Enable the modulator
        GPIO.output (22, True)
        # keep enabled for a period
        time.sleep(0.25)
        # Disable the modulator
        GPIO.output (22, False)

    def socket1(self, state):
        """Switch socket 1 on/off."""
        if state == 1:
            self._transmit(self.codes["socket1"]["ON"])
        elif state == 0:
            self._transmit(self.codes["socket1"]["OFF"])

    def cleanup_gpio(self):
        GPIO.cleanup()

    def initialise_gpio(self):
        # set the pins numbering mode
        GPIO.setmode(GPIO.BOARD)

        # Select the GPIO pins used for the encoder K0-K3 data inputs
        GPIO.setup(11, GPIO.OUT)
        GPIO.setup(15, GPIO.OUT)
        GPIO.setup(16, GPIO.OUT)
        GPIO.setup(13, GPIO.OUT)

        # Select the signal to select ASK/FSK
        GPIO.setup(18, GPIO.OUT)

        # Select the signal used to enable/disable the modulator
        GPIO.setup(22, GPIO.OUT)

        # Disable the modulator by setting CE pin lo
        GPIO.output (22, False)

        # Set the modulator to ASK for On Off Keying 
        # by setting MODSEL pin lo
        GPIO.output (18, False)

        # initialise_gpio K0-K3 inputs of the encoder to 0000
        GPIO.output (11, False)
        GPIO.output (15, False)
        GPIO.output (16, False)
        GPIO.output (13, False)

def main():
    state = int(argv[1])

    with Energenie() as e:
        e.socket1(state)

if __name__ == "__main__":
    main()

