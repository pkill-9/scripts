#!/bin/sh -e

#https://github.com/NixOS/nixpkgs/issues/9415#issuecomment-139655485
#
#After talking about this with @ttuegel for a while, we've established that the following works:
#
#Install patchelf (nix-env -i patchelf)
#Copy the system libGL.so.1 (usually in /usr/lib64) somewhere, eg. ~/.libgl-shim/libGL.so.1.
#patchelf --set-rpath /lib:/usr/lib:/lib64:/usr/lib64 ~/.libgl-shim/libGL.so.1 (adjust lib paths if necessary for your distribution)
#Replicate other libGL.so.* symlinks that your system has, but in your ~/.libgl-shim, and pointing to your modified copy.
#Run openarena as follows: LD_LIBRARY_PATH=~/.libgl-shim openarena
#This approach also seems automatable, and should work for other things that use makeWrapper :)
#
#I'm still working on something, but once I've gotten further with that, I'll probably do a proper writeup of this on my blog or something.

if [ $# -eq 0 ]
    then
        echo "No arguments supplied. Supply library to register with Nix packages."
	exit 1
        fi

# Copy the lib.so file (or the file it symlinks to) and patch it
cp /usr/lib64/$1.so ~/.nix-libs
patchelf --set-rpath /lib:/usr/lib:/lib64:/usr/lib64 ~/.nix-libs/$1.so
# create symlinks of all lib.so.* files to it
find /usr/lib64 -name "$1.so.*" -exec sh -c "ln -s $1.so ~/.nix-libs/\$(basename {})" \;
