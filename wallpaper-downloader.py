#!/usr/bin/env python
### Download wallpapers from DesktopNexus.com ###

from bs4 import BeautifulSoup as Bsoup
from bs4 import SoupStrainer
import requests
import re

def dl_wallpaper(id):
    """Download wallpaper and return it"""

    id = str(id)
    url = "https://dncache-mauganscorp.netdna-ssl.com/cropped-wallpapers/{}/{}-1440x900-[DesktopNexus.com].jpg".format(id[:4], id)
    return requests.get(url).content

def scrape_wallpaper_list(url):
    """Return list of IDs of wallpapers in the page of given url"""
    s = re.compile("thumbnail-[0-9]+")
    strainer = SoupStrainer("img", id=s)

    page = requests.get(url).content
    imgs = Bsoup(page, "lxml", parse_only=strainer).find_all("img", id=s)
    return {img["id"].replace("thumbnail-", "") : img["alt"] for img in imgs}
    #return [img["id"].replace("thumbnail-", "") for img in imgs]

if __name__ == "__main__":
    category = "nature"
    url = "https://{}.desktopnexus.com/".format(category)
    ids = scrape_wallpaper_list(url)
    id_list = list(ids.keys())
    print(list(ids.values()))
    #id_list = list(scrape_wallpaper_list(url).keys())

    with open("/tmp/{}.jpg".format(ids[id_list[1]]), "wb") as f:
        f.write(dl_wallpaper(id_list[1]))
